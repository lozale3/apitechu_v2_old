package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Array;
import java.util.ArrayList;

@SpringBootApplication
@RestController
public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechuApplication.class, args);
		ApitechuApplication.productModels = ApitechuApplication.getTestData();
	}

	private static ArrayList<ProductModel> getTestData(){
		ArrayList<ProductModel> produtsModel = new ArrayList<>();

		produtsModel.add(new ProductModel(
				"1",
				"Producto1",
				10
		));
		produtsModel.add(new ProductModel(
				"2",
				"Producto2",
				20.2f
		));
		produtsModel.add(new ProductModel(
				"3",
				"Producto3",
				30.3f
		));
		return produtsModel;
	}

	/*@RequestMapping("/hello")
	public String greetings(){
		return "Hola Mundo desde API Tech U!";
	}*/
}
