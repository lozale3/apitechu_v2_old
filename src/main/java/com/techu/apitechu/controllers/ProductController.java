package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * Cualquier petición a nuestra API, ir a /apitechu/v1
 */
@RestController
public class ProductController {
    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");
        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("id es: " + id);

        ProductModel result = new ProductModel();

        for(ProductModel product: ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                result = product;
            }
        }
        return result;
        //return new ProductModel();
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("la id del nuevo producto es: " + newProduct.getId());
        System.out.println("la descripción del nuevo producto es: " + newProduct.getDesc());
        System.out.println("el precio del nuevo producto es: " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
        //return new ProductModel();
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("la id en parámetro de la url es: " + id);
        System.out.println("la id del producto a actualizar es: " + product.getId());
        System.out.println("la descripción del producto a actualizar es: " + product.getDesc());
        System.out.println("el precio del producto a actualizar es: " + product.getPrice());

        for(ProductModel productList: ApitechuApplication.productModels){
            if(productList.getId().equals(id)){
                productList.setId(product.getId());
                productList.setDesc((product.getDesc()));
                productList.setPrice(product.getPrice());
            }
        }

        return product;
        //return new ProductModel();
    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id){
        System.out.println("patchProduct");
        System.out.println("la id en parámetro de la url es: " + id);
        System.out.println("la descripción del producto a actualizar es: " + productData.getDesc());
        System.out.println("el precio del producto a actualizar es: " + productData.getPrice());

        ProductModel result = new ProductModel();

        for(ProductModel productInList: ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                result = productInList;

                if(productData.getDesc()!=null) productInList.setDesc(productData.getDesc());
                if(productData.getPrice()>0) productInList.setPrice(productData.getPrice());
                //if(productData.getPrice()!=0.0) productList.setPrice(productData.getPrice());
            }
        }
        return result;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("la id del producto a borrar es: " + id);

        ProductModel result = new ProductModel();

        for(ProductModel product: ApitechuApplication.productModels){
            if(product.getId().equals(id)){
                ApitechuApplication.productModels.remove(product);

                result = product;
            }
        }

        return result;
        //return new ProductModel();
    }
}
